#!/bin/bash

# ------------------------------------------------------------------------------
# Inspect given attributes to start script logic
# ------------------------------------------------------------------------------
if [ -z "$1" ]
then
  echo "Please provide your extension file name (without .jar)"
  exit 1
fi

if [[ $1 = *".jar" ]]; then
  echo "Please omit the .jar just set the name as paramter here"
  exit 1
fi

file=$1".jar"
if [ ! -f "$file" ]
then
  echo "$0: File '${file}' not found."
  exit 1
fi


appdatename=$1"-"`date +%Y%m%d%H%M%S`

# first undeploy otherwise we will run into some problems here
./bpmExtensionUndeploy.sh $1

# copy latest build to extension folder
docker cp $1.jar abas-bpm-camunda:/camunda/thirdparty/$appdatename.jar

# deploys extension
docker exec -i abas-bpm-camunda sh -c 'sh /camunda/bin/deploy-camunda-jar.sh '$appdatename'.jar'
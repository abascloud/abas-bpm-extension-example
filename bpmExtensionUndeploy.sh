#!/bin/bash

# ------------------------------------------------------------------------------
# Inspect given attributes to start script logic
# ------------------------------------------------------------------------------
if [ -z "$1" ]
then
  echo "Please provide your extension file name (without .jar)"
  exit 1
fi

if [[ $1 = *".jar" ]]; then
  echo "Please omit the .jar just set the name as paramter here"
  exit 1
fi

file=$1".jar"
if [ ! -f "$file" ]
then
  echo "$0: File '${file}' not found."
  exit 1
fi

docker exec -i abas-bpm-camunda sh -c 'sh /camunda/bin/undeploy-camunda-jar.sh '$1'*.jar'
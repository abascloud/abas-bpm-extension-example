package de.abas.bpm.example;
import java.util.logging.Logger;

/**
 * Example how a java delegate can look like
 */
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class ToUppercaseDelegate implements JavaDelegate {
        private final static Logger LOGGER = Logger.getLogger(ToUppercaseDelegate.class.getName());
        
        @Override
        public void execute(DelegateExecution execution) throws Exception {
                String input = (String) execution.getVariable("input");
                LOGGER.info("Input variable: "+input);
                input = input.toUpperCase();
                execution.setVariable("input", input);
        }
}

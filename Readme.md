# abas bpm extensions

## General information

This repo shows, how you can create an abas bpm extension by adding a JavaDelegate, new bpm file and an element template and deploys it directly into your abas-bpm-camunda docker image.
See also here (https://docs.camunda.org/manual/7.7/user-guide/process-engine/delegation-code/#java-delegate)

## Remarks

- Take care to use a proper namespace for your JavaDelegates.
- You can undeploy your abas bpm extension, but you have to keep in mind, that other running processes could still use one of your Java Delegates.
- Works only with a running abas-bpm-camunda docker container > 0.113.0

## Build

```bash
# Create extension jar
./gradlew jar
```

## Local deployment (on camunda machine)

```bash
# Deletes all old deployed extensions (extension-name*.jar) and deploys your extension
./gradlew deploy

# Undeploys the extension
./gradlew undeploy
```

## Remote deployment

```bash
# Copies your created jar file, the script `bpmExtensionDeploy.sh` and `bpmExtensionunDeploy.sh` to remote installation
./gradlew jar && scp -r build/libs/abas-bpm-custom-example.jar *.sh USERNAME@REMOTEHOST:.

# Deletes all old deployed extensions (extension-name*.jar) and deploys your extension
./bpmExensionDeploy.sh [jar_name_without_.jar]

# Undeploys the extension
./bpmExensionUnDeploy.sh [jar_name_without_.jar]
```

## Undeployment of bpmn files
Within this example `resources/META-INF/processes.xml` sets _isDeleteUponUndeploy_ to false so you have to undeploy your provided bpm examples before or after you undeployed the abas bpm extension. Use the camunda admin cockpit to get this done (see here: https://docs.camunda.org/manual/7.7/webapps/cockpit/deployment-view/).

If you want to remove all of your custom bpm process instances while undeploying your extension you have to set  _isDeleteUponUndeploy_ to true.